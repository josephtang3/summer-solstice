import scipy.optimize
import astropy.units as u
import astropy.time
import astropy.coordinates

astropy.coordinates.solar_system_ephemeris.set("de441_part-2")

time_solstice = astropy.time.Time(
    scipy.optimize.root_scalar(
        f=lambda t: (astropy.coordinates.get_body("sun", time=astropy.time.Time(t, format="unix")).tete.ra - 90 * u.deg).value,
        bracket=[
            astropy.time.Time("2023-06-20").unix,
            astropy.time.Time("2023-06-23").unix,
        ],
    ).root,
    format="unix",
)

print(time_solstice.iso)
